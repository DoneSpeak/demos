package cn.donespeak.demos.grpcspringbootstarter;

import java.io.ByteArrayInputStream;

import org.apache.tomcat.util.http.fileupload.ByteArrayOutputStream;
import org.junit.jupiter.api.Test;

import cn.donespeak.demos.grpcspringbootstarter.pojo.vo.UserVo;

public class UserVoUserCardSerializationTest {

	@Test
	public void test() throws Exception {
		UserVo.UserCard card = UserVo.UserCard.newBuilder()
			.setName("Bob")
			.setAge(15)
			.setId(12345)
			.build();
		
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		card.writeTo(output);
		
		byte[] bytes = output.toByteArray();
		
		ByteArrayInputStream input = new ByteArrayInputStream(bytes);
		UserVo.UserCard cartRecovered = UserVo.UserCard.parseFrom(input);
		
		System.out.println(cartRecovered);
		System.out.println(cartRecovered.getId());
		System.out.println(cartRecovered.getName());
		System.out.println(cartRecovered.getAge());
		
	}
}
