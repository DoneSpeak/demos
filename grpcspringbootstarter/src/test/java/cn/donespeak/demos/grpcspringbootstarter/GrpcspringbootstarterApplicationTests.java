package cn.donespeak.demos.grpcspringbootstarter;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class GrpcspringbootstarterApplicationTests {

	@Test
	void contextLoads() {
		CloseableHttpClient httpclient = null;
		CloseableHttpResponse response = null;
		
		try {
			httpclient = HttpClients.createDefault();
			HttpGet httpGet = new HttpGet("http://127.0.0.1:8080/users/123");
			response = httpclient.execute(httpGet);
			
			HttpEntity entity = response.getEntity();
			System.out.println(response.getStatusLine().getStatusCode());
			if(entity != null) {
				System.out.println("Response content: " + (entity == null? null: EntityUtils.toString(entity)));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(httpclient != null) {
				try {
					httpclient.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(response != null) {
				try {
					response.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
