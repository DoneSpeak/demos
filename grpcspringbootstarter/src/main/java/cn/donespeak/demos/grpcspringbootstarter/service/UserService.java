package cn.donespeak.demos.grpcspringbootstarter.service;

import java.util.List;

import cn.donespeak.demos.grpcspringbootstarter.pojo.vo.UserVo;
import cn.donespeak.demos.grpcspringbootstarter.pojo.vo.UserVo.UserCard;
import cn.donespeak.demos.grpcspringbootstarter.pojo.vo.UserVo.UserCardList;

public interface UserService {
	UserCardList listUsers();

	UserCard getById(long userId);

	UserCard createUser(UserCard user);
}