package cn.donespeak.demos.grpcspringbootstarter.service.Impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.donespeak.demos.grpcspringbootstarter.pojo.vo.UserVo;
import cn.donespeak.demos.grpcspringbootstarter.pojo.vo.UserVo.UserCard;
import cn.donespeak.demos.grpcspringbootstarter.pojo.vo.UserVo.UserCardList;
import cn.donespeak.demos.grpcspringbootstarter.service.UserService;

@RestController
@RequestMapping("/users")
public class UserServiceImpl implements UserService {

	@Override
	public UserCardList listUsers() {
		UserVo.UserCard card1 = UserVo.UserCard.newBuilder()
				.setId(100)
				.setAge(12)
				.setName("Bob").build();
		return UserVo.UserCardList.newBuilder().addUserCards(card1).build();
	}

	@Override
	public UserCard getById(long userId) {
		return UserVo.UserCard.newBuilder()
			.setId(100)
			.setAge(12)
			.setName("Bob").build();
	}

	@Override
	public UserCard createUser(UserCard user) {
		System.out.println("id: " + user.getId());
		System.out.println("name: " + user.getName());
		System.out.println("age: " + user.getAge());
		return user;
	}	
}
