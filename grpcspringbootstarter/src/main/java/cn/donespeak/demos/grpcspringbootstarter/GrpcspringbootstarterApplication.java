package cn.donespeak.demos.grpcspringbootstarter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GrpcspringbootstarterApplication {

	public static void main(String[] args) {
		SpringApplication.run(GrpcspringbootstarterApplication.class, args);
	}

}
