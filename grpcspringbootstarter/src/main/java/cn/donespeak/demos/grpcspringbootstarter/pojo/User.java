package cn.donespeak.demos.grpcspringbootstarter.pojo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {
	private long userId;
	private String name;
	private int age;
}