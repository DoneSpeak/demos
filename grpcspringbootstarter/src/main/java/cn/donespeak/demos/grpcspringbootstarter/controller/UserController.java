package cn.donespeak.demos.grpcspringbootstarter.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.donespeak.demos.grpcspringbootstarter.config.ProtoRestController;
import cn.donespeak.demos.grpcspringbootstarter.pojo.vo.UserVo;
import cn.donespeak.demos.grpcspringbootstarter.pojo.vo.UserVo.UserCardList;
import cn.donespeak.demos.grpcspringbootstarter.service.UserService;

//@RestController
//@RequestMapping(value = "/users", produces = "application/x-protobuf")
@ProtoRestController
public class UserController {

	@Autowired
	private UserService userService;
	
	@GetMapping("")
	public UserCardList listUsers() {
		return userService.listUsers();
	}
	
	@GetMapping("/{userId:\\d+}")
	public UserVo.UserCard getUser(@PathVariable long userId) {
		return userService.getById(userId);
	}
	
	/*
	 * 配置了messageConverter之后，这里要求请求的content-type 是 application/x-protobuf
	 */
	@PostMapping("")
	public UserVo.UserCard createUser(@RequestBody UserVo.UserCard user) {
		return userService.createUser(user);
	}
}
