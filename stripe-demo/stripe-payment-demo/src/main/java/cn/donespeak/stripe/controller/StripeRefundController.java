package cn.donespeak.stripe.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.donespeak.stripe.service.StripeRefundService;

/**
 * @author guanrongYang
 * @date 2019/03/07 17:40:52
 */
@RestController
@RequestMapping("/stripe/refunds")
public class StripeRefundController {
	
	@Autowired
	private StripeRefundService stripeRefundService;
	
	@PostMapping("")
	public Object refund(Map<String, String> params) {
		String token = params.get("stripeToke");
		return null;
	}
}
