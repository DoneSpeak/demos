package cn.donespeak.stripe.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.donespeak.stripe.service.StripeService;

/**
 * @author guanrongYang
 * @date 2019/03/05 17:18:21
 */
@RestController
@RequestMapping("/stripe")
public class StripeChargeController {
	
	@Autowired
	StripeService stripeService;
	
	@GetMapping("/charge")
	public Map<String, Object> hello(@RequestBody Map<String, Object> param) {
		System.out.println("============");
		System.out.println(param);
		System.out.println("============");
		return param;
	}
	
	// http://keep.01ue.com/?pi=352329&_a=app&_c=index&_m=p
	@PostMapping("/charge")
	public Object charge(@RequestParam Map<String, Object> param) {
		System.out.println("============");
		System.out.println(param);
		System.out.println("============");
		
		stripeService.charge(String.valueOf(param.get("stripeToken")), 100, false);
		return param;
	}

	// http://keep.01ue.com/?pi=352329&_a=app&_c=index&_m=p
	@PostMapping("/charge-more")
	public Object chargeWithMoreInfo(@RequestParam Map<String, Object> param) {
		System.out.println("============");
		System.out.println(param);
		System.out.println("============");
		
		stripeService.chargeWithMoreInfo(String.valueOf(param.get("stripeToken")), 1258, true);
		return param;
	}
	
	@PostMapping("/charge-capture")
	public Object chargeCapture(@RequestParam Map<String, Object> param) {
		System.out.println("============");
		System.out.println(param);
		System.out.println("============");
		
		stripeService.charge(String.valueOf(param.get("stripeToken")), 1111, true);
		return param;
	}

	
	@PostMapping("/charge-meta")
	public Object chargeMeta(@RequestParam Map<String, Object> param) {
		System.out.println("============");
		System.out.println(param);
		System.out.println("============");
		
		stripeService.chargeWithMeta(String.valueOf(param.get("stripeToken")));
		return param;
	}
	
	@PostMapping("/capture")
	public Object capture(@RequestBody Map<String, Object> params) {
		System.out.println("============");
		System.out.println(params);
		System.out.println("============");
		
		return stripeService.capture(String.valueOf(params.get("stripeToken")));
	}
}