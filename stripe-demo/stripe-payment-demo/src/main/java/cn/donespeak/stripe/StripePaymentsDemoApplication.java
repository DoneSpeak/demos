package cn.donespeak.stripe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

import com.stripe.Stripe;

import cn.donespeak.stripe.common.Settings;

@SpringBootApplication(exclude=SecurityAutoConfiguration.class)
public class StripePaymentsDemoApplication {

	static {
		Stripe.apiKey = Settings.STRIPE_API_S_KEY;
	}

	public static void main(String[] args) {
		SpringApplication.run(StripePaymentsDemoApplication.class, args);
	}
}