package cn.donespeak.stripe.common;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.ParseException;
import org.apache.http.StatusLine;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;

import com.alibaba.fastjson.JSONObject;

public class HttpClientUtil {
	// time out
	private static final int SOCKET_TIMEOUT = 7000;
	private static final int CONNECT_TIMEOUT = 5000;
	private static final int CONNECTION_REQUEST_TIMEOUT = 5000;

	// max conections
	private static final int HTTP_MAXTOTAL = 200;
	private static final int DEFAULT_MAX_PER_ROUTE = 20;

	private static CloseableHttpClient client;
	
	/**
	 * configuration for request
	 */
	private static RequestConfig requestConfig;

	/**
	 * connection pool
	 */
	private static PoolingHttpClientConnectionManager cm;

	private synchronized static CloseableHttpClient getClient() {
		if (client == null) {
			synchronized (CloseableHttpClient.class) {
				if (client == null) {
					client = HttpClients.custom().setConnectionManager(getPooling()).build();
				}
			}
			return client;
		}
		return client;
	}

	private synchronized static RequestConfig getRequestConfig() {
		if (requestConfig == null) {
			synchronized (RequestConfig.class) {
				if (requestConfig == null) {
					requestConfig = RequestConfig.custom().setSocketTimeout(SOCKET_TIMEOUT) // longest data transfer time
							.setConnectTimeout(CONNECT_TIMEOUT) // longest connection time
							.setConnectionRequestTimeout(CONNECTION_REQUEST_TIMEOUT) // longest time get connection from connection poll
							.build();
				}
			}
		}
		return requestConfig;
	}

	private synchronized static PoolingHttpClientConnectionManager getPooling() {
		if (cm == null) {
			synchronized (PoolingHttpClientConnectionManager.class) {
				if (cm == null) {
					cm = new PoolingHttpClientConnectionManager();
					cm.setMaxTotal(HTTP_MAXTOTAL); // reset the max connection num to 200
					cm.setDefaultMaxPerRoute(DEFAULT_MAX_PER_ROUTE); // reset the default max connection num for per route　to 200
				}
			}
		}
		return cm;
	}

	private static HttpGet getHttpGetObject(String uri, Map<String, String> headers) {
		HttpGet get = new HttpGet(uri);
		get.setConfig(getRequestConfig());
		if(headers != null) {
			for(Entry<String, String> entry: headers.entrySet()) {
				get.setHeader(entry.getKey(), entry.getValue());				
			}
		}
		return get;
	}

	private static HttpPost getHttpPostObject(String url, Map<String, String> headers) {
		HttpPost post = new HttpPost(url);
		post.setConfig(getRequestConfig());
		if(headers != null) {
			for(Entry<String, String> entry: headers.entrySet()) {
				post.setHeader(entry.getKey(), entry.getValue());				
			}
		}
		
		return post;
	}

	public static String sendHttpGetRequest(String uri, Map<String,Object> paramMap, Map<String, String> headers) throws ParseException, IOException {
		String queryString = "";
		if(paramMap != null && paramMap.size() > 0) {
			queryString = getParamQueryString(paramMap);
		}
		return sendHttpGetRequest(uri, queryString, headers);
	}

	public static String sendHttpGetRequest(String uri, String queryString, Map<String, String> headers) throws ParseException, IOException {
		HttpGet httpGet = null;
		String returnString = null;
		queryString = (queryString == null || queryString.isEmpty()) ? "": "?" + queryString;
		try {
			httpGet = getHttpGetObject(uri + queryString, headers);
			
			CloseableHttpResponse response = HttpClientUtil.getClient().execute(httpGet);
			StatusLine sline = response.getStatusLine();
			int statusCode = sline.getStatusCode();
			if (statusCode == 200) {
				returnString = EntityUtils.toString(response.getEntity());
			} else {
			}
		} finally {
			httpGet.releaseConnection();
		}
		return returnString;
	}
	
	public static String sendHttpPostRequest(String uri, Map<String,Object> paramMap, Map<String, String> headers) throws Exception {
        // add request data into the request
        String json = JSONObject.toJSONString(paramMap);
        return sendHttpPostRequest(uri, json, headers);
	}
	
	public static String sendHttpPostRequest(String uri, String json, Map<String, String> headers) throws Exception {
		HttpPost post = getHttpPostObject(uri, headers);
		StringEntity myEntity = new StringEntity(json, ContentType.APPLICATION_JSON);
        post.setEntity(myEntity);
        // post.addHeader("content-type", "application/json");
        String returnString = null;
        try {
            CloseableHttpResponse response = HttpClientUtil.getClient().execute(post);
            StatusLine sline = response.getStatusLine();
            int statusCode = sline.getStatusCode();
            if (statusCode == 200) {
            	returnString = EntityUtils.toString(response.getEntity());
            } else {
            	String msg = "Send post request failed to " + uri + ", statusCode: " + statusCode;
                throw new Exception(msg);
            }
        } finally {
            post.releaseConnection();
        }
        return returnString;
	}
	
	private static String getParamQueryString(Map<String,Object> paramMap) {
		if(paramMap == null) {
			return null;
		}
		StringBuffer buf = new StringBuffer();
		if(paramMap.size() == 0) {
			return buf.toString();
		}
		
		buf.append("?");
		for(Map.Entry<String,Object> entry: paramMap.entrySet()) {
			buf.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
		}
		return buf.toString();
	}
}
