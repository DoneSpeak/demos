package cn.donespeak.stripe.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.stripe.exception.StripeException;
import com.stripe.model.Refund;

import cn.donespeak.stripe.service.StripeRefundService;

/**
 * @author guanrongYang
 * @date 2019/03/07 17:44:03
 */
@Service("stripeRefundService")
public class StripeRefundServiceImpl implements StripeRefundService {

	@Override
	public Object refundAll(String stripeToken) {
		Map<String, Object> params = new HashMap<>();
		params.put("charge", stripeToken);
		try {
			Refund refund = Refund.create(params);
			System.out.println(refund);
			return "Success";
		} catch (StripeException e) {
			e.printStackTrace();
			return new Exception(e);
		}
	}

	@Override
	public Object refundPart(String stripeToken) {
		Map<String, Object> params = new HashMap<>();
		params.put("charge", stripeToken);
		// 单位是分
		params.put("amount", 1000);
		try {
			Refund refund = Refund.create(params);
			System.out.println(refund);
			return "Success";
		} catch (StripeException e) {
			e.printStackTrace();
			return new Exception(e);
		}
	}
}
