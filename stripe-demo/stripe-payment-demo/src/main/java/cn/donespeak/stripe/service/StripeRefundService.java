package cn.donespeak.stripe.service;

/**
 * @author guanrongYang
 * @date 2019/03/07 17:43:50
 */
public interface StripeRefundService {

	public Object refundAll(String stripeToken);
	
	public Object refundPart(String stripeToken);
}
