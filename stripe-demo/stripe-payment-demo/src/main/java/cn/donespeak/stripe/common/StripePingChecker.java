package cn.donespeak.stripe.common;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.ParseException;

/**
 * @author guanrongYang
 * @date 2019/03/08 20:20:16
 */
public class StripePingChecker {

	public static String[] StripeDomains = {
			"api.stripe.com",
			"checkout.stripe.com",
			"js.stripe.com",
			"m.stripe.com",
			"q.stripe.com"
	};
	
	public static List<String> getIpAddressesByUrlSource() {
		String ipAddressesUri = "https://stripe.com/files/ips/ips_api.txt";
		return getIpsByUrlSource(ipAddressesUri);
	}
	
	public static List<String> getIpsByUrlSource(String url) {
		try {
			String result = HttpClientUtil.sendHttpGetRequest(url, new HashMap<String, Object>(), null);
			return parseIps(result);
		} catch (ParseException | IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static List<String> getWebhookIpsByUrlSource() {
		String webhookUri = "https://stripe.com/files/ips/ips_webhooks.txt";
		return getIpsByUrlSource(webhookUri);
	}
	
	public static Map<String, Boolean> checkDomains() throws IOException {
		return checkIps(Arrays.asList(StripeDomains));
	}
	
	/**
	 * check the accessability of Ip addresses that api.stripe.com may resolve to.
	 * @throws IOException 
	 */
	public static Map<String, Boolean> checkApiIpAddresses() throws IOException {
		List<String> ips = getIpAddressesByUrlSource();
		return checkIps(ips);
	}
	
	public static Map<String, Boolean> checkWebhookIps() throws IOException {
		List<String> ips = getWebhookIpsByUrlSource();
		return checkIps(ips);
	}
	
	private static Map<String, Boolean> checkIps(List<String> ips) throws IOException {
		Map<String, Boolean> fails = new HashMap<String, Boolean>();
		for(String ip: ips) {
			fails.put(ip, isReachable(ip));
		}
		return fails;
	}

	public static void check() throws IOException {
//		Map<String, Boolean> domains = checkDomains();
//		System.out.println("###### Check Domains:");
//		for(Entry<String, Boolean> entry: domains.entrySet()) {
//			System.out.println("## " + entry.getKey() + ": " + entry.getValue());
//		}
//		
//		Map<String, Boolean> ips = checkApiIpAddresses();
//		System.out.println("###### Check IPs:");
//		for(Entry<String, Boolean> entry: ips.entrySet()) {
//			System.out.println("## " + entry.getKey() + ": " + entry.getValue());
//		}

		Map<String, Boolean> webhooks = checkWebhookIps();
		System.out.println("###### Check webhooks:");
		for(Entry<String, Boolean> entry: webhooks.entrySet()) {
			System.out.println("## " + entry.getKey() + ": " + entry.getValue());
		}
	}
	
	public static boolean isReachable(String remoteInetAddr) throws IOException {
        boolean reachable = false;
        InetAddress address = InetAddress.getByName(remoteInetAddr);
        reachable = address.isReachable(1500);
        return reachable;
    }
	
	private static List<String> parseIps(String ipsLinked) throws IOException {
		 List<String> ips = new ArrayList<String>();
		 try(BufferedReader reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(ipsLinked.getBytes())))) {
			 String ip;
			 while((ip = reader.readLine()) != null) {
				 ips.add(ip);
			 }
		 }
		return ips;
	}
	
	public static void main(String[] args) throws IOException {
		System.out.println("================ START ================");
		check();
	}
}
