package cn.donespeak.stripe.common;

/**
 * @author guanrongYang
 * @date 2019/03/05 16:08:47
 */
public interface Settings {
	public static String STRIPE_API_P_KEY = "pk_test_yourkey";
	public static String STRIPE_API_S_KEY = "sk_test_yourkey";
}
