package cn.donespeak.stripe.service;

/**
 * @author guanrongYang
 * @date 2019/03/05 17:34:22
 */
public interface StripeService {
	
	void charge(String stripeToken, int amount, boolean capture);

	/**
	 * @param valueOf
	 */
	Object capture(String valueOf);

	/**
	 * @param stripeToken
	 * @return
	 */
	Object chargeWithMeta(String stripeToken);

	/**
	 * @param stripeToken
	 * @return
	 */
	Object chargeWithCheck(String stripeToken);

	/**
	 * @param stripeToken
	 * @param capture
	 */
	void chargeWithMoreInfo(String stripeToken, int amount, boolean capture);
}
