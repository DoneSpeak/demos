package cn.donespeak.stripe.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.stripe.Stripe;
import com.stripe.exception.ApiConnectionException;
import com.stripe.exception.ApiException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.IdempotencyException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.exception.PermissionException;
import com.stripe.exception.RateLimitException;
import com.stripe.exception.SignatureVerificationException;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;

import cn.donespeak.stripe.common.Settings;
import cn.donespeak.stripe.service.StripeService;

/**
 * @author guanrongYang
 * @date 2019/03/05 17:35:51
 */
@Service("stripeService")
public class StripeServiceImpl implements StripeService {

	@Override
	public void charge(String stripeToken, int amount, boolean capture) {

		Map<String, Object> params = new HashMap<>();
		params.put("amount", amount);
		params.put("currency", "usd");
		params.put("receipt_email", "guanrong.young@gmail.com");
		params.put("description", "Example charge");
		params.put("statement_descriptor", "GR descriptor");
		params.put("source", stripeToken);
		params.put("capture", capture);
		try {
			Charge charge = Charge.create(params);
			System.out.println(charge);
		} catch (StripeException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Object capture(String stripeToken) {
		try {
			Charge charge = Charge.retrieve(stripeToken);
			Charge chargeResult = charge.capture();

			System.out.println(chargeResult);
			return "SUCCESS";
		} catch (StripeException e) {
			e.printStackTrace();
			return e;
		}
	}

	@Override
	public Object chargeWithMeta(String stripeToken) {
		// https://stripe.com/docs/charges#storing-information-in-metadata
		// 使用该meta的时候, 在使用radar时, 需要设置一些规则
		// https://stripe.com/docs/radar/rules/reference#metadata-attributes
		Map<String, Object> params = new HashMap<>();
		params.put("amount", 999);
		params.put("currency", "usd");
		params.put("description", "Example charge");
		params.put("source", stripeToken);
		Map<String, String> metadata = new HashMap<>();
		metadata.put("refer", "shop.bees360.com");
		metadata.put("order_id", "6735");
		params.put("metadata", metadata);
		try {
			Charge charge = Charge.create(params);
			System.out.println(charge);
			return "SUCCESS";
		} catch (StripeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return e;
		}
	}

	@Override
	public Object chargeWithCheck(String stripeToken) {
		// https://stripe.com/docs/charges#storing-information-in-metadata
		// 使用该meta的时候, 在使用radar时, 需要设置一些规则
		// https://stripe.com/docs/radar/rules/reference#metadata-attributes
		Map<String, Object> params = new HashMap<>();
		params.put("amount", 999);
		params.put("currency", "usd");
		params.put("description", "Example charge");
		params.put("source", stripeToken);
		Map<String, String> metadata = new HashMap<>();
		metadata.put("refer", "shop.bees360.com");
		metadata.put("order_id", "6735");
		params.put("metadata", metadata);
		try {
			Charge charge = Charge.create(params);
			System.out.println(charge);
			return "SUCCESS";
		} catch (StripeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return e;
		}
	}

	@Override
	public void chargeWithMoreInfo(String stripeToken, int amount, boolean capture) {

		Map<String, Object> params = new HashMap<>();
		params.put("amount", amount);
		params.put("currency", "usd");
		params.put("receipt_email", "guanrong.young@gmail.com");
		params.put("description", "Example charge");
		params.put("statement_descriptor", "GR descriptor");
		params.put("source", stripeToken);
		params.put("capture", capture);

		params.put("shipping", getShipping());
		params.put("metadata", getMetadata());

		Charge charge = chargeWithHandlingException(params);
		
		System.out.println("######## chargeWithMoreInfo ######");
		System.out.println(charge);
	}

	/**
	 * Visit https://stripe.com/docs/api/errors to see the error list
	 * @param params
	 * @return
	 */
	private Charge chargeWithHandlingException(Map<String, Object> params) {
		try {
			// Use Stripe's library to make requests...
			Charge charge = Charge.create(params);
			return charge;
		} catch (ApiConnectionException e) {
			// Network communication with Stripe failed
			System.out.println("code: " + e.getCode());
			System.out.println("Message: " + e.getMessage());
			System.out.println("request-id: " + e.getRequestId());
			e.printStackTrace();
		} catch (ApiException e) {
			System.out.println("code: " + e.getCode());
			System.out.println("Message: " + e.getMessage());
			System.out.println("request-id: " + e.getRequestId());
			e.printStackTrace();
			// API errors cover any other type of problem (e.g., a temporary
			// problem with Stripe's servers), and are extremely uncommon.
		} catch (AuthenticationException e) {
			System.out.println("code: " + e.getCode());
			System.out.println("Message: " + e.getMessage());
			System.out.println("request-id: " + e.getRequestId());
			e.printStackTrace();
			// Authentication with Stripe's API failed
			// (maybe you changed API keys recently)
		} catch (CardException e) {
			e.printStackTrace();
			// Since it's a decline, CardException will be caught
			System.out.println("code: " + e.getCode());
			System.out.println("Message: " + e.getMessage());
			System.out.println("request-id: " + e.getRequestId());
		} catch (IdempotencyException e) {
			System.out.println("code: " + e.getCode());
			System.out.println("Message: " + e.getMessage());
			System.out.println("request-id: " + e.getRequestId());
			e.printStackTrace();
			// Idempotency errors occur when an Idempotency-Key is re-used on a
			// request that does not match the first request's API endpoint and
			// parameters.
		} catch (RateLimitException e) {
			System.out.println("code: " + e.getCode());
			System.out.println("Message: " + e.getMessage());
			System.out.println("request-id: " + e.getRequestId());
			e.printStackTrace();
			// Too many requests made to the API too quickly
		} catch (InvalidRequestException e) {
			System.out.println("code: " + e.getCode());
			System.out.println("Message: " + e.getMessage());
			System.out.println("request-id: " + e.getRequestId());
			e.printStackTrace();
			// Invalid parameters were supplied to Stripe's API
		} catch (SignatureVerificationException e) {
			System.out.println("code: " + e.getCode());
			System.out.println("Message: " + e.getMessage());
//			System.out.println("request-id: " + e.getRequestId());
			e.printStackTrace();
			// Errors triggered by our client-side libraries when failing to
			// validate fields (e.g., when a card number or expiration date is
			// invalid or incomplete).
		} catch (StripeException e) {
			System.out.println("code: " + e.getCode());
			System.out.println("Message: " + e.getMessage());
			System.out.println("request-id: " + e.getRequestId());
			// Display a very generic error to the user, and maybe send
			// yourself an email
			e.printStackTrace();
		} catch (Exception e) {
			// Something else happened, completely unrelated to Stripe
			e.printStackTrace();
		}
		return null;
	}

	private Map<String, Object> getShipping() {
		Map<String, Object> shipping = new HashMap<String, Object>();

		Map<String, Object> address = new HashMap<String, Object>();
		address.put("line1", "Mistery Street");
		address.put("city", "Houston");
		address.put("state", "TX");
		address.put("country", "US");
		// address.put("line2", ""); 属性值不应该设置为空字符串, 会报错, 可以不设置, 也可以设置为null以表示不设置
		// address.put("line2", null);
		address.put("postal_code", "40141");

		shipping.put("address", address);
		shipping.put("name", "GR-Yang");
		shipping.put("phone", "15986781861");

		return shipping;
	}

	private Map<String, Object> getMetadata() {
		Map<String, Object> metadata = new HashMap<String, Object>();
		metadata.put("orderId", 14454542);
		metadata.put("userId", 1000124);

		return metadata;
	}
	
	public static void main(String[] args) {
		// 测试 https://stripe.com/docs/testing
		StripeService stripeService = new StripeServiceImpl();
		Stripe.apiKey = Settings.STRIPE_API_S_KEY;
		// 校验需要在 Radar 中设定规则才能生效 https://dashboard.stripe.com/test/radar/rules
		// 拿到token之后, 后端无法在进行校验 https://stackoverflow.com/questions/39448503/verify-cvc-code-before-creating-charge
		// 生成charge之后, 会含有check信息
//		stripeService.chargeWithMoreInfo("ch_1EAaFdCX05bH3bMBCP4pYoZF", 1000, true);
//		stripeService.chargeWithMoreInfo("tok_cvcCheckFail", 1000, true);
		stripeService.chargeWithMoreInfo("tok_cvcCheckFaddil", 1000, true);
	}
}
