package cn.donespeak.demos.squareupbasesdk;

import com.squareup.connect.ApiClient;
import com.squareup.connect.ApiException;
import com.squareup.connect.Configuration;
import com.squareup.connect.api.TransactionsApi;
import com.squareup.connect.auth.OAuth;
import com.squareup.connect.models.ListTransactionsResponse;

/**
 * https://github.com/square/connect-java-sdk/blob/master/docs/TransactionsApi.md#listTransactions
 * @author yangguanrong
 *
 */
public class ListTransactionsTest {

	public static void main(String[] args) {
		ApiClient defaultClient = Configuration.getDefaultApiClient();

		// Configure OAuth2 access token for authorization: oauth2
		OAuth oauth2 = (OAuth) defaultClient.getAuthentication("oauth2");
		oauth2.setAccessToken("sandbox-sq0atb-bdqf6XBXyHb9sM3ExOtnQA");

		TransactionsApi apiInstance = new TransactionsApi();
		String locationId = "CBASEBhFhWYD45axA7Qr8EW405ggAQ"; // String | The ID of the location to list transactions for.
		String beginTime = null; // String | The beginning of the requested reporting period, in RFC 3339 format.  See [Date ranges](#dateranges) for details on date inclusivity/exclusivity.  Default value: The current time minus one year.
		String endTime = null; // String | The end of the requested reporting period, in RFC 3339 format.  See [Date ranges](#dateranges) for details on date inclusivity/exclusivity.  Default value: The current time.
		String sortOrder = null; // String | The order in which results are listed in the response (`ASC` for oldest first, `DESC` for newest first).  Default value: `DESC`
		String cursor = null; // String | A pagination cursor returned by a previous call to this endpoint. Provide this to retrieve the next set of results for your original query.  See [Pagination](/basics/api101/pagination) for more information.
		try {
		    ListTransactionsResponse result = apiInstance.listTransactions(locationId, beginTime, endTime, sortOrder, cursor);
		    System.out.println(result);
		} catch (ApiException e) {
		    System.err.println("Exception when calling TransactionsApi#listTransactions");
		    e.printStackTrace();
		}
	}
}
