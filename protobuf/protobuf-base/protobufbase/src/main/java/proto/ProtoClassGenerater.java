package proto;

import java.io.File;
import java.io.IOException;

public class ProtoClassGenerater {

	public static void main(String[] args) {
		process();
	}
	
	private static void process() {
		String projectPath = new File("").getAbsolutePath();
		String protoPath = new File(projectPath, "src").getAbsolutePath();
		String javaOut = new File(projectPath, "/src/main/java").getAbsolutePath();
		String protfileDir = new File(projectPath, "/src/main/java/proto").getAbsolutePath();
		
		String[] protofiles = new String[] {
				"person.proto"
		};
		
		for(String protof: protofiles) {
			
			String cmd = "protoc --proto_path=" + protoPath + " --java_out=" + javaOut + " " + new File(protfileDir, protof).getAbsolutePath();
			try {
				System.out.println("[command-start] " + cmd);
				Runtime.getRuntime().exec(cmd);
				System.out.println("[command-end] " + cmd);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
