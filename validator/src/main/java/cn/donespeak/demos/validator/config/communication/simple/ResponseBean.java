package cn.donespeak.demos.validator.config.communication.simple;

import java.util.List;

import org.springframework.http.HttpStatus;

import cn.donespeak.demos.validator.config.communication.withhttpstatus.ApiValidatorError.FieldValidatorError;

/**
 * @author guanrongYang
 * @date 2019/03/24 19:35:43
 */
public class ResponseBean {
	private int code;
	private Object data;
	private String msg;
	
	public ResponseBean() {		
	}
	
	public ResponseBean(int code, String msg) {
		this.code = code;
		this.msg = msg;
	}
	
	public ResponseBean(int code, Object data, String msg) {
		this(code, msg);
		this.data = data;
	}

	public static ResponseBean success(Object data) {
		return new ResponseBean(0, data, "Error occurs");
	}

	public static ResponseBean systemError() {
		return new ResponseBean(HttpStatus.INTERNAL_SERVER_ERROR.value(), "error occurs");
	}
	
	public static ResponseBean validatorError(List<FieldValidatorError> data) {
		return new ResponseBean(HttpStatus.UNPROCESSABLE_ENTITY.value(), data, "Validation failed");
	}
		
	//** getter and setter **//
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	@Override
	public String toString() {
		return "ResponseBean [code=" + code + ", data=" + data + ", msg=" + msg + "]";
	}
}
