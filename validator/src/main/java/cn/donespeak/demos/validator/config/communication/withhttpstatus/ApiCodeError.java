package cn.donespeak.demos.validator.config.communication.withhttpstatus;

import org.springframework.http.HttpStatus;

/**
 * @author guanrongYang
 * @date 2019/03/24 16:46:24
 */
public class ApiCodeError extends ApiError {
	protected String code;
	
	public ApiCodeError() {
		super();
	}
	
	public ApiCodeError(HttpStatus status, String code, String msg) {
		super(status, msg);
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
