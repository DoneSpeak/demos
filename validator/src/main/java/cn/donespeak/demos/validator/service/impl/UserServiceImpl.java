package cn.donespeak.demos.validator.service.impl;

import org.springframework.stereotype.Service;

import cn.donespeak.demos.validator.pojo.User;
import cn.donespeak.demos.validator.service.UserService;

/**
 * @author guanrongYang
 * @date 2019/03/24 17:34:09
 */
@Service("userService")
public class UserServiceImpl implements UserService {

	@Override
	public User getByUserId(long userId) {
		User user = new User();
		user.setUserId(userId);
		user.setAge(18);
		user.setName("xiaoming");
		user.setEmail("xiaoming@foxmail.com");
		user.setPhone("");
		
		return user;
	}
	
	@Override
	public User addUser(User user) {
		user.setUserId(10001);
		System.out.println("#### " + user);
		return user;
	}
}
