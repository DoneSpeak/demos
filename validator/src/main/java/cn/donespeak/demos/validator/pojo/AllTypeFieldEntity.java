 package cn.donespeak.demos.validator.pojo;

import java.util.Date;

import lombok.Data;

/**
 * @author Guanrong Yang
 * @date 2019/06/27
 */
@Data
public class AllTypeFieldEntity {

    private int intValue;
    private long longValue;
    private short shortValue;
    private byte byteValue;
    private boolean boolValue;
    
    private Integer intPValue;
    private Long longPValue;
    private Short shortPValue;
    private Byte bytePValue;
    private Boolean boolPValue;
    
    private double doubleValue;
    private float floatValue;
    
    private Double doublePValue;
    private Float floatPValue;
    
    private String stringValue;
    
    private Date dateValue;
    
    @Data
    public static class Klass {
        private String field;
    }
}
