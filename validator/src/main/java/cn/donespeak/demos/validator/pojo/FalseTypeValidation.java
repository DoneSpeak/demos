 package cn.donespeak.demos.validator.pojo;

import javax.validation.constraints.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.*;


import lombok.Data;

/**
 * @author Guanrong Yang
 * @date 2019/06/27
 */
 @Data
public class FalseTypeValidation {
     
//    @Min(10)      // javax.validation.UnexpectedTypeException: HV000030: No validator could be found for constraint 'javax.validation.constraints.Min' validating type 'cn.donespeak.demos.validator.pojo.FalseTypeValidation$AKlass'. Check configuration for 'ak'
//    @Max(1000)    // javax.validation.UnexpectedTypeException: HV000030: No validator could be found for constraint 'javax.validation.constraints.Max' validating type 'cn.donespeak.demos.validator.pojo.FalseTypeValidation$AKlass'. Check configuration for 'ak'
//    @AssertFalse
//    @AssertTrue
//    @Future
//    @Past
//    @NotNull
//    @Null
//    @Pattern(regexp="[a-z]+")
//    @Size(min=10)
//    @Range(min=10)
//    @Email
//    @Length(min=10)
//    @NotBlank
    // @NotEmpty // javax.validation.UnexpectedTypeException: HV000030: No validator could be found for constraint 'javax.validation.constraints.NotEmpty' validating type 'cn.donespeak.demos.validator.pojo.FalseTypeValidation$AKlass'. Check configuration for 'ak'
    private AKlass ak;
    
    @Min(10)
    @Max(100)
    private double a;
    
    @Range(min=10, max=100)
    private int range;

    @Data
    public static class AKlass {
        private String a;
    }
}
