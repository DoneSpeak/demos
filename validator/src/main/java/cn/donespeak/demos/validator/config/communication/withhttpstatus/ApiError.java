package cn.donespeak.demos.validator.config.communication.withhttpstatus;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author guanrongYang
 * @date 2019/03/24 16:39:08
 */
public class ApiError {
	protected HttpStatus status;
	protected String msg;
	
	public ApiError() {
	}
	public ApiError(HttpStatus status, String msg) {
		this.status = status;
		this.msg = msg;
	}
	
	@JsonIgnore
	public HttpStatus getStatus() {
		return status;
	}
	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
}