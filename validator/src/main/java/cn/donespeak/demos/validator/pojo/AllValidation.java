 package cn.donespeak.demos.validator.pojo;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.*;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import lombok.Data;

/**
 * @author Guanrong Yang
 * @date 2019/06/27
 */
@Data
public class AllValidation {
   
    @Min(100)
    private int min;
    @Max(1000)
    private int max;
    
    @Min(100)
    private Integer minP;
    @Max(1000)
    private Integer maxP;
    
    @AssertTrue
    private boolean boolTrue;
    @AssertFalse
    private boolean boolFalse;
    
    @AssertTrue
    private Boolean boolPTrue;
    @AssertFalse
    private Boolean boolPFalse;
    
    @Future
    private Date future;
    @Past
    private Date past;
    
    @NotNull
    @Null
    private int notNullInt;
    
    @NotNull
    private String notNull;
    
    @Null
    private String nullValue;
    
    @Pattern(regexp = "[a-z]+")
    private String patternValue;
    
    @Size(min=10, max=100)
    private List<String> size;
    
    @Email
    private String email;
    
    @Length(min=10,max=100)
    private String length;
    
    @NotBlank
    private String notBlank;
    
    @NotEmpty
    private List<String> notEmpty;
    
    @Range(min=10)
    private long range;  

    @Valid
    private OtherKlass other;
    
    @Digits(integer=2, fraction = 0)
    private long digits;
    
    @Digits(integer=2, fraction = 3)
    private String digitsString;
        
    @Positive
    private int positive;
    
    @PositiveOrZero
    private int positiveOrZero;
    
    @Negative
    private int negative;

    @NegativeOrZero
    private int negativeOrZero;
    
    @Data
    public static class OtherKlass {
        @NotNull
        private String a;
        @Min(10)
        private int b;
    }
}
