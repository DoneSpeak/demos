package cn.donespeak.demos.validator.config.communication.withhttpstatus;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpStatus;

/**
 * @author guanrongYang
 * @date 2019/03/24 16:46:58
 */
public class ApiValidatorError extends ApiError {
	
	public static class FieldValidatorError {
		private String type;
		private String field;
		private String message;
		
		public FieldValidatorError() {			
		}
		
		public FieldValidatorError(String code, String field, String message) {
			this.type = code;
			this.field = field;
			this.message = message;
		}
		
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public String getField() {
			return field;
		}
		public void setField(String field) {
			this.field = field;
		}
		public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
	}
	
	protected List<FieldValidatorError> errors;

	private static final HttpStatus HTTP_STATUS_UNPROCESSABLE_ENTITY = HttpStatus.UNPROCESSABLE_ENTITY;
	private static final String DEFAULT_MESSAGE = "Validation failed";
	
	public ApiValidatorError() {
		super(HTTP_STATUS_UNPROCESSABLE_ENTITY, DEFAULT_MESSAGE);
	}

	public ApiValidatorError(List<FieldValidatorError> errors) {
		this();
		this.errors = errors;
	}
	
	public ApiValidatorError(FieldValidatorError error) {
		this(Arrays.asList(error));
	}
	
	//** getter and setter **//
	public List<FieldValidatorError> getErrors() {
		return errors;
	}

	public void setErrors(List<FieldValidatorError> errors) {
		this.errors = errors;
	}
}