package cn.donespeak.demos.validator.service;

import cn.donespeak.demos.validator.pojo.User;

/**
 * @author guanrongYang
 * @date 2019/03/24 17:33:52
 */
public interface UserService {

	User getByUserId(long userId);

	User addUser(User user);
}