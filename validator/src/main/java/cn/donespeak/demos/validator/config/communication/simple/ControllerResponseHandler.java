package cn.donespeak.demos.validator.config.communication.simple;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

public class ControllerResponseHandler implements ResponseBodyAdvice<Object> {
	
	private Logger logger = LogManager.getLogger(getClass());

	/*
	 * It will supports all return types.
	 */
	@Override
	public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
		logger.debug("converterType " + converterType);
		logger.debug("returnType: " + returnType.getClass());
		logger.debug("MappingJackson2HttpMessageConverter is assignable from converterType: "
				+ MappingJackson2HttpMessageConverter.class.isAssignableFrom(converterType));
		return true;
	}

	/*
	 * package the return data into the data field of <code>ResponseJson</code>
	 */
	@Override
	public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType,
			Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request,
			ServerHttpResponse response) {
		if(body instanceof ResponseBean) {
			return body;
		} else {
			// 所有没有返回　ResponseBean　结构的结果均认为是成功的
			ResponseBean.success(body);
		}
		return body;
	}
}