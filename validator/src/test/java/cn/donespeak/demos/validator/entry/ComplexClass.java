 package cn.donespeak.demos.validator.entry;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

/**
 * @author Guanrong Yang
 * @date 2019/06/28
 */
@Data
public class ComplexClass {

    @NotEmpty
    public List<@Valid AClass> classList;
    
    public Map<@NotBlank String, @Valid BClass> stringMapClass;
    
    @NotNull
    public Map<@Valid AClass, @Valid BClass> classMapClass;
    
    @NotNull
    public Set<@Valid AClass> classSet;
}
