 package cn.donespeak.demos.validator.entry;

import javax.validation.constraints.Min;

import lombok.Data;

/**
 * @author Guanrong Yang
 * @date 2019/06/28
 */
@Data
public class BClass {

    @Min(10)
    private int intV;
}
