 package cn.donespeak.demos.validator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.hibernate.validator.HibernateValidator;
import org.junit.Test;

import cn.donespeak.demos.validator.pojo.AllValidation;
import cn.donespeak.demos.validator.pojo.AllValidation.OtherKlass;

/**
 * @author Guanrong Yang
 * @date 2019/06/26
 */
public class ValidateNullValueTest {
    
    private Validator validator = validator();

    @Test
    public void testNullValueField() {
        AllValidation allValidation = new AllValidation();
        allValidation.setOther(null);
        allValidation.setLength("df");
        allValidation.setMin(-10000);
        allValidation.setMax(100000000);
        allValidation.setBoolFalse(true);
        allValidation.setBoolTrue(false);
        allValidation.setNotBlank("");
        allValidation.setNotEmpty(Collections.emptyList());
        allValidation.setDigits(1000);
        allValidation.setDigitsString("da");
        allValidation.setFuture(new Date());
        allValidation.setPast(new Date());
        allValidation.setRange(1);
        allValidation.setSize(Arrays.asList("d", "f"));
        allValidation.setEmail("df");
        allValidation.setNullValue("");
        allValidation.setPatternValue("45454");
        allValidation.setPositive(-1);
        allValidation.setNegative(10);
        allValidation.setPositiveOrZero(-1);
        allValidation.setNegativeOrZero(10);
        
        Set<ConstraintViolation<AllValidation>> constraints = validator.validate(allValidation);
        
        for(ConstraintViolation<AllValidation> c: constraints) {
            System.out.println("### annotation: " + c.getConstraintDescriptor().getAnnotation().annotationType());
            System.out.println("### property path: " + c.getPropertyPath());
            System.out.println("### invalid value: " + c.getInvalidValue());
            System.out.println("### message: " + c.getMessage());
            System.out.println("### =========================================");
        }
    }
    
    public Validator validator() {
        return Validation
            .byProvider(HibernateValidator.class)
            .configure()
            // 快速返回模式，有一个验证失败立即返回错误信息
            // 设置为 false，表示需要所有的校验都完成为止才返回
            .failFast(false)
            .buildValidatorFactory()
            .getValidator();
    }
}
