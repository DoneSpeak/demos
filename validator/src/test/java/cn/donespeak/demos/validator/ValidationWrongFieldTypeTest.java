 package cn.donespeak.demos.validator;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.hibernate.validator.HibernateValidator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.donespeak.demos.validator.config.ValidatorConfig;
import cn.donespeak.demos.validator.pojo.FalseTypeValidation;

/**
 * @author Guanrong Yang
 * @date 2019/06/26
 */
 @RunWith(SpringJUnit4ClassRunner.class)
public class ValidationWrongFieldTypeTest {

    private Validator validator = validator();

    @Test
    public void test() {
        
        FalseTypeValidation falseType = new FalseTypeValidation();
        falseType.setA(1000);
        falseType.setRange(-9);
        
        Set<ConstraintViolation<FalseTypeValidation>> constraints = validator.validate(falseType);
        
        for(ConstraintViolation<FalseTypeValidation> c: constraints) {
            System.out.println("### property path: " + c.getConstraintDescriptor().getAnnotation());
            System.out.println("### property path: " + c.getPropertyPath());
            System.out.println("### invalid value: " + c.getInvalidValue());
            System.out.println("### message: " + c.getMessage());
            System.out.println("### =========================================");
        }
        System.out.println("### ================== END =================");
    }
    
    public Validator validator() {
        return Validation
            .byProvider(HibernateValidator.class)
            .configure()
            // 快速返回模式，有一个验证失败立即返回错误信息
            // 设置为 false，表示需要所有的校验都完成为止才返回
            .failFast(false)
            .buildValidatorFactory()
            .getValidator();
    }
}
