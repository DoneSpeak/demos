 package cn.donespeak.demos.validator.entry;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.Data;

/**
 * @author Guanrong Yang
 * @date 2019/06/28
 */
@Data
public class AClass {
    
    public AClass() {
        
    }
    
    public AClass(String name) {
        this.name = name;
    }
    
    @Length(min=10)
    @NotNull
    private String name;
}
