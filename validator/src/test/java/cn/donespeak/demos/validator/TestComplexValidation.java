 package cn.donespeak.demos.validator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.donespeak.demos.validator.config.ValidatorConfig;
import cn.donespeak.demos.validator.entry.AClass;
import cn.donespeak.demos.validator.entry.BClass;
import cn.donespeak.demos.validator.entry.ComplexClass;

/**
 * @author Guanrong Yang
 * @date 2019/06/28
 */
 @RunWith(SpringJUnit4ClassRunner.class)
 @ContextConfiguration(classes= {ValidatorConfig.class})
public class TestComplexValidation {

    @Autowired
    private Validator validator;
    
    @Test
    public void test() {
        ComplexClass klass = new ComplexClass();
        Map<AClass, BClass> abs = new HashMap<AClass, BClass>();
        abs.put(null, new BClass());
        abs.put(new AClass("m--d"), new BClass());
        klass.setClassMapClass(abs);
        
        List<AClass> aclasses = new ArrayList<AClass>();
        aclasses.add(new AClass("ddd"));
        aclasses.add(new AClass());
        klass.setClassList(aclasses);
        
        Set<AClass> aset = new HashSet<AClass>();
        aset.add(new AClass("dfd"));
        aset.add(new AClass());
        klass.setClassSet(aset);
        
        Map<String, BClass> stringMapClass = new HashMap<String, BClass>();
        stringMapClass.put(null, new BClass());
        stringMapClass.put("dfdf==ss", null);
        klass.setStringMapClass(stringMapClass);
        
        Set<ConstraintViolation<ComplexClass>> constraints = validator.validate(klass);
        
        for(ConstraintViolation<ComplexClass> c: constraints) {
            System.out.println("### annotation: " + c.getConstraintDescriptor().getAnnotation().annotationType());
            System.out.println("### property path: " + c.getPropertyPath());
            System.out.println("### invalid value: " + c.getInvalidValue());
            System.out.println("### message: " + c.getMessage());
            System.out.println("### =========================================");
        }
    }
}
